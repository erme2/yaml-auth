<?php
/*
 * This file is part of the yaml-auth package.
 *
 * (c) Arduino Di Giosia <me@erme2.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace yamlAuth;

/**
 * A bridge between the bin file and the other class
 *
 * @author Arduino Di Giosia <me@erme2.com>
 */
class Console
{
    protected $command = false;
    protected $username = false;
    protected $password = false;
    protected $argument = false;

    public function __construct(array $argv)
    {
        if(isset($argv[1])) {
            $this->command = $argv[1];
        }
        if(isset($argv[2])) {
            $this->username = $argv[2];
        }
        if(isset($argv[3])) {
            $this->password = $argv[3];
        }
        if(isset($argv[4])) {
            $this->argument = $argv[4];
        }
    }

    public function exec()
    {
        switch ($this->command) {
            case 'create':
                $function = 'add';
                break;
            case 'delete':
                $function = 'delete';
                break;
            case 'list':
                $function = 'browse';
                break;
            case 'update':
                $function = 'edit';
                break;
            default:
                throw new \Exception('invalid function requested ('.$this->command.')');
        }

        try {
            $this->run($function);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    public function run($function)
    {
        $userObj = new User();
        switch ($function) {
            case 'browse':
                try {
                    $message = $userObj->browse();
                } catch (\Exception $exception) {
                    $message = "ERROR: ".$exception->getMessage();
                }
                break;
            default:
                try {
                    $message = $userObj->{$function}($this->username, $this->password, $this->argument);
                } catch (\Exception $exception) {
                    $message = "ERROR: ".$exception->getMessage();
                }
        }
        echo $message;
    }
}