<?php
/*
 * This file is part of the yaml-auth package.
 *
 * (c) Arduino Di Giosia <me@erme2.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace yamlAuth;


/**
 * BREAD User management class
 *
 * @author Arduino Di Giosia <me@erme2.com>
 */
class User
{
    const ARCHIVE_NAME= 'yaml-auth-users.yml';
    const USER_TYPES = [
        'user' => 0,
        'admin' => 1,
    ];
    const DEFAULT_USER_TYPE = 0;

    private $yamlHelper;
    private $validator;

    public function __construct()
    {
        $this->validator = new UserValidation();
        try {
            $this->yamlHelper = new yamlHelper(self::ARCHIVE_NAME);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }


    public function browse($returnHtml = false) {
        $list = $this->yamlHelper->browse();

        if(count($list) > 0) {
            $userTypes = array_flip(self::USER_TYPES);
            echo "@ ".count($list)." users found\n";

            foreach ($list as $user) {
                echo " - ".$userTypes[$user['type']]." # ".$user['username']."\n";
            }
        } else {
            echo "@ No users found\n";
        }
    }

    public function edit(string $username, string $password, string $type) {

        try {
            $this->validateUserData($username, $password, $type);
        } catch (\Exception $exception) {
            throw $exception;
        }
        $user = $this->yamlHelper->read('username', $username);

        if($user && is_array($user)) {
            $updatedRecord = [
                'username' => $username,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'type' => self::USER_TYPES[$type],
                'created' => isset($user['created']) ? $user['created'] : time(),
                'last_access' => isset($user['last_access']) ? $user['last_access'] : time(),
            ];

            if($this->yamlHelper->save($updatedRecord) === false) {
                throw new \Exception(" user $username update failed");
            } else {
                return "User ".$username." updated\n";
            }
        } else {
            throw new \Exception("$username not present - use create function to create user");
        }
    }

    /**
     * add new user
     *
     * @return string
     * @throws \Exception
     */
    public function add(string $username, string $password, string $type) {
        try {
            $this->validateUserData($username, $password, $type);
        } catch (\Exception $exception) {
            throw $exception;
        }

        $user = $this->yamlHelper->read('username', $username);

        if($user) {
            throw new \Exception($username." already present, try to update.\n");
        } else {
            $createRecord = [
                'username' => $username,
                'password' => password_hash($password, PASSWORD_DEFAULT),
                'type' => self::USER_TYPES[$type],
                'created' => time(),
                'last_access' => 0,
            ];

            if($this->yamlHelper->save($createRecord) === false) {
                throw new \Exception(" user $username create failed");
            } else {
                return "User ".$username." created\n";
            }
        }
    }

    public function delete(string $username) {

        $validator = new UserValidation();
        try {
            $validator->isValidUsername($username);
        } catch (\Exception $exception) {
            throw $exception;
        }

        $user = $this->yamlHelper->read('username', $username);
        if($user) {
            $deleted = $this->yamlHelper->delete('username', $username);
        } else {
            throw new \Exception("User $username not found\n");
        }

        if($deleted) {
            echo "@ User $username deleted\n";
        } else {
            throw new \Exception("User $username not found/not deleted");
        }
    }

    private function validateUserData(string $username, string $password, string $userType)
    {
        $validator = new UserValidation();

        try {
            $validator->isValidUsername($username);
        } catch (\Exception $exception) {
            throw $exception;
        }
        try {
            $validator->isValidPassword($password);
        } catch (\Exception $exception) {
            throw $exception;
        }

        try {
            $validator->isValidUserType($userType);
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    private function write($fileContent)
    {
        return file_put_contents(self::FILE_NAME, $fileContent);
    }

    private function rebuildDump($dump) {
        $return = [];
        foreach ($dump as $record) {
            array_push($return, $record);
        }
        return $return;
    }

    private function toData()
    {
        return [
            'username' => $this->username,
            'password' => $this->password,
            'type' => $this->type,
            'created' => $this->created,
            'last_access' => $this->last_access,
        ];
    }
}
