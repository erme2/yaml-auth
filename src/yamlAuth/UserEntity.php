<?php
namespace yamlAuth;


class UserEntity
{
    protected $username;
    protected $password;
    protected $type;
    protected $created;
    protected $last_access;
    protected $is_new;

    public function __construct(array $data)
    {
        if(isset($data['username']) && $this->validator->isValidUsername($data['username'])) {
            $yamlData = $this->yamlHelper->read('username', $data['username']);

            if($yamlData) {
                $this->username = $yamlData['username'];
                $this->password = $yamlData['password'];
                $this->type = $yamlData['type'];
                $this->created = $yamlData['created'];
                $this->last_access = $yamlData['last_access'];
                $this->is_new = false;
            } else {
                $this->username = $data['username'];
                if($this->validator->isValidPassword($data['password'])) {
                    $this->password = password_hash($data['password'], PASSWORD_DEFAULT);
                } else {
                    throw new \Exception("Invalid password");
                }
                if($this->validator->isValidUserType($data['type'])) {
                    $this->type = self::USER_TYPES[$data['type']];
                } else {
                    $this->type = self::DEFAULT_USER_TYPE;
                }
                $this->created = time();
                $this->last_access = 0;
                $this->is_new = true;
            }
            return $this;
        } else {
            throw new \Exception("Invalid username");
        }
    }

}