<?php
/*
 * This file is part of the yaml-auth package.
 *
 * (c) Arduino Di Giosia <me@erme2.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace yamlAuth;

/**
 * User validation class
 *
 * @author Arduino Di Giosia <me@erme2.com>
 */
class UserValidation
{
    const MIN_PASSWORD_LENGTH = 6;

    public function isValidUsername(string $username)
    {
        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        throw new \Exception('Invalid username/email');
    }

    public function isValidPassword(string $password)
    {
        if(strlen($password) >= self::MIN_PASSWORD_LENGTH) {
            return true;
        }
        throw new \Exception('Invalid password - too short');
    }

    public function isValidUserType(string $userType)
    {
        $userTypeNames = [];
        foreach(User::USER_TYPES as $type=>$id) {
            array_push($userTypeNames, $type);
            if($userType == $type) {
                return true;
            }
        }
        throw new \Exception('Invalid user type - '.$userType.' ['.implode(', ', $userTypeNames).']');
    }
}