<?php
namespace yamlAuth;

use Symfony\Component\Yaml\Yaml;

class yamlHelper
{
    protected $archiveName;
    protected $fileName;

    public function __construct(string $archiveName)
    {
        $this->fileName = getcwd()."/$archiveName";
        if(file_exists($this->fileName)) {
            $this->archiveName = $archiveName;
        } else {
            $create = $this->write([], $this->fileName);
            if($create) {
                // nothing to do
                $this->archiveName = $archiveName;
            } else {
                throw new \Exception("$archiveName - invalid directory or file");
            }
        }
    }

    public function browse()
    {
        // todo implement this function to add query params
        return $this->getAll();
    }

    public function read(string $key, string $value)
    {
        try {
            $records = $this->getAll();
        } catch (\Exception $exception) {
            throw $exception;
        }

        if(is_array($records)) {
            // nothing to do
        } else {
            $records = [];
        }

        foreach ($records as $record) {
            if($record[$key] == $value) {
                return $record;
            }
        }
        return false;
    }

    public function delete(string $key, string $value)
    {
        $return = false;
        try {
            $records = $this->getAll();
        } catch (\Exception $exception) {
            throw $exception;
        }

        foreach ($records as $k => $record) {
            if($record[$key] == $value) {
                unset($records[$k]);
                $return = true;
            }
        }

        if($return) {
            $write = [];
            foreach ($records as $record) {
                array_push($write, $record);
            }
            $records = $write;
        }

        $this->write($records);
        return $return;
    }

    public function save($data)
    {
        try {
            $records = $this->getAll();
        } catch (\Exception $exception) {
            throw $exception;
        }
        array_push($records, $data);
        return $this->write($records);
    }

    private function write(array $data, $fileName = false)
    {
        return file_put_contents(
            $fileName ? $fileName : $this->fileName,
            Yaml::dump($data)
        );
    }

    private function getAll()
    {
        if(file_exists($this->fileName)) {
            try {
                $return = Yaml::parseFile($this->fileName);
            } catch (\Exception $exception) {
                throw $exception;
            }
        } else {
            $return = [];
        }
        // if the file not exists we will return an empty array
        return $return;
    }


}